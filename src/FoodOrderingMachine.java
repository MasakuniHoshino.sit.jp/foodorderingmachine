import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane OrderedItemsList;
    private JButton checkOutButton;
    private JLabel Totalprice;

    int total=0;

    public FoodOrderingMachine() {
        tempuraButton.setIcon(new ImageIcon( this.getClass().getResource("tempura_image.jpg")
        ));
        karaageButton.setIcon(new ImageIcon( this.getClass().getResource("karaage_image.jpg")
        ));
        gyozaButton.setIcon(new ImageIcon( this.getClass().getResource("gyoza_image.jpg")
        ));
        udonButton.setIcon(new ImageIcon( this.getClass().getResource("udon_image.jpg")
        ));
        yakisobaButton.setIcon(new ImageIcon( this.getClass().getResource("yakisoba_image.jpg")
        ));
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("ramen_image.jpg")
        ));
        Totalprice.setText("Total "+total+"yen");
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",100);

            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",150);
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza",200);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",50);
            }
        });
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba",300);
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",400);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout ?",
                        "checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0){
                    JOptionPane.showMessageDialog (null , "Thank you. The total price is " +total +" yen.");
                    total=0;
                    OrderedItemsList.setText(" ");
                    Totalprice.setText("Total "+total+"yen");
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " +food +"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation==0){
            String currentText = OrderedItemsList.getText();
            OrderedItemsList.setText(currentText+food+" "+price+"yen"+"\n");
            total+=price;
            Totalprice.setText("Total "+total+"yen");
            JOptionPane.showMessageDialog (null , "Thank you for Ordering " +food +"! It will be served as soon as possible.");
        }
    }
}
